# SNR Klasyfikacja ikon aplikacji webowych i mobilnych.
# Mikołaj Ciesielski, Paweł Paczuski

## Zadanie oraz zbiór danych
Otrzymaliśmy zadanie klasyfikacji ikon (200x200) pochodzących ze stron internetowych oraz aplikacji mobilnych. Zbiór danych zawierał w sobie 153488 grafik. 
Klasy zostały oddzielone od siebie za pomocą katalogów, które nazwane były jak klasy.

### Podział zbioru na dane trenujące, walidujące oraz testowe
Za pomocą skryptu `preprocess_data.py` dodatkowo podzieliliśmy dane na zbiory treningowy, 
walidujący i testowy w stosunku 6:2:2. 

Różnych klas w zbiorze danych jest 105, co obniża znacząco oczekiwania wobec jakości
ostatecznego klasyfikatora (łatwiej jest wydawać decyzję między klasami A i B, niż gdy jest ich 105).
Gdyby losowo przydzielać klasy do ocenianych ikon, to uzyskalibyśmy dokładność około 0.95%. 
Z drugiej strony dzięki temu faktowi dużo łatwiej jest oceniać, czy dane podejście poprawia tworzony model.

W wyniku przeprowadzonej analizy i prób uczenia tworzonych przez nas sieci w ramach projektu,
doszliśmy do wniosku, że dostarczony zbiór danych jest zle zbalansowany. Wśród klas były takie,
w których liczba ikon nie przekraczała 10.

W związku z tym zdecydowaliśmy się na odpowiednie przygotowanie otrzymanego zbioru, aby dało się
w sensowny sposób wykonać trenowanie sieci neuronowych.

Ostatecznie liczbę klas zmniejszyliśmy do 47. Wśród usuniętych klas znalazły się zbiory zawierające
niewiele przypadków oraz takie, które przedstawiały bardzo podobne obiekty, które często były
nierozróżnialne dla człowieka.

## Keras, python, numpy 
Keras to biblioteka służaca do szybkiego i wygodnego eksperymentowania ze sztucznymi sieciami 
neuronowymi. Jej głównym celem jest stworzenie łatwo rozszerzalnego zbioru abstrakcji obniżających
kognitywne obciążenie osób zajmujących sie sieciami neuronowmi, szczególnie głębokimi sieciami neuronowymi.
Keras może używać pod maską różnych "backendów" do wykonywania obliczeń matematycznych. Naszym wyborem był TensorFlow 
– niezwykle popularny zbiór operacji do przetwarzania symbolicznego w matematyce oraz pakiet narzędzi przydatnych dla
uczenia maszynowego 

Keras napisany jest w Pythonie – dynamicznym języku obiektowym wysokiego poziomu. Python znany jest w programistycznej 
społeczności jako język posiadający mnóstwo łatwych w obsłudze bibliotek pozwalających wykonywać trudne operacje za pomocą
prostego API. Keras bardzo dobrze wpisuje się w filozofię tego stosu technologicznego oferując duże możliwości przy użyciu
prostych koncepcyjnie struktur.

Dla obliczeń wykonywanych pomiędzy kolejnymi wywołaniami Kerasa część operacji na wektorach wykonywaliśmy za pomocą NumPy – 
biblioteki oferującej szybkość oraz uproszczenie wykonywania operacji na danych tablicowych.


## Programistyczne koncepty których użyliśmy podczas wykonywania projektu

### VGG net
Keras zawiera kilkanaście architektur sprawdzonych w przeróżnych zadaniach związanych z deep learningiem sieci. Oprócz architektur,
załączone są dla nich wagi uzyskane dzieki trenowaniu na dużych zbiorach danych, np. ImageNet. Podczas wykonywania zadań,
opieraliśmy nasze eksperymenty na sieci VGG-16 niezbyt głębokiej sieci splotowej do której wprowadzaliśmy modyfikacje opisane w zadaniach. 

### ImageDataGenerator
Ograniczcenia posiadanej prze nas mocy obliczeniowej nie pozwalały na załadowanie całego zbioru danych do pamięci komputera,
jednak problem został łatwo rozwiązany za pomocą abstrakcji oferowanej przez Kerasa – ImageDataGeneratorów. Dzięki nim 
dostarczanie danych do sieci odbywa się w przyjaznych dla pamięci RAM `chunkach` przez co nie jest konieczne wczytywanie
na raz całego zbioru danych do pamięci komputera. Dodatkowo, zastosowanie tego mechanizmu ułatwia podział daych ze zbioru
według klas, gdyż opisana wczesniej struktura folderów jest wspierana przez ImageDataGeneratory.

### Ocena jakości rozwiązań
W przypadku, gdy klas jest więcej niż 2 nie jest możliwe zastosowanie klasycznej krzywej ROC. Możliwe byłoby wygenerowanie
wszystkich par klas w posiadanym zbiorze i następnie dla nich kreślenie tej krzywej, jednak jest to problematyczne pod 
wzlędem liczby wykresów, które byłyby potrzebne.
Zastosowaliśmy wobec tego 

## Napotkane problemy podczas rozwiązywania zadania
W czasie realizacji projektu napotkaliśmy się na kilka problemów, część z nich dało się rozwiązac,
natomiast pozostałe były bezpośrednią przyczyną zmniejszenia się jakości treningu sieci neuronowych.

Większość problematycznych kwestii wiązało się z dostarczonym zbiorem danych, o którego
niezbyt przyjaznych cechach, w kontekscie uczenia maszynowego, wspomnieliśmy na początku dokumentu.

Pierwszym problemem, który dostrzegliśmy był fakt,
że niektóre klasy zawarte w zbiorze danych zawierały bardzo podobne ikony.
Okazało się nawet, że podobieństwo obiektów reprezentowanych przez zdjęcia zawarte w różnych klasach
jest na tyle duże, że sami mieliśmy trudności dokonać decyzji co do przynależności obiektu
do odpowiedniej klasy.

Drugą kwestią, która przyczyniła się do niższej oceny jakości otrzymanych przez nas danych
był szum, który w tych danych występował. Zdarzało się że obiekty reprezentujące pewną klasę
tak naprawdę reprezentowały obiekty innej klasy.

Poniżej dwa przykłady obiektu z klasy "samolot".
Jeśli chodzi o pierwszy rysunek, raczej nikt nie ma wątpliwości co do tego co on reprezentuje,
natomiast drugi pozostawie wiele do życzenia.

![image info](images/plane.jpg) ![image info](images/bad_plane.jpg)

Po niezbyt długiej analizie, większośc osób jest wstanie dostrzec kolejną wadę zbioru danych.
Większość rysunków ma znacznie różniące się rozmiary oraz stosunek długości boków.
Przyczyną tego jest fakt, że nie każdy rysunek niesie taką samą informację.
Przekłada się to bezpośrednio na to, iż klasy które zawierają wiecej obiektów mogą zawierać mniejszą ilość informacji od tych, które mają ich mniej.

W celu poprawy jakości uczenia zdecydowaliśmy, że niezbędne jest odpowiednie
przekształcenie dostarczonego zbioru danych. Przekształcenie to polegało na
usunięciu klas o bardzo małej liczbie zdjęć oraz takich, które zawierały obiekty
bardzo podobne do innych klas.
## Zadanie 1

Pierwsze zadanie polegało na klasyfikacji przetwarzanych danych za pomocą niezbyt głębokiej struktury dostarczanej przez Kerasa z dodaną
własną warstwą sieci neuronowej kompletnie połączonej. Wybraliśmy VGG-16 gdyż była ona mniej głęboka niż reszta ocenianych 
sieci. 

Ostatnia warstwa była klasycznną warstwą sieci neuronowych – połączona kompletnie, aktywowana funkcją sigmoidalną posiadająca
tyle neuronów co klas w zbiorze – 105. Uczenie sieci (*uczyliśmy tylko doaną przez nas warstwę*)
 było kontrolowane przez funkcję oceny opartą o root-means-sqared.
Domyślne tempo uczenia (lr==0.01) okazało sie zbyt duże, ustawiliśmy lr=2e-5. Świadczy to dużo o analizowanej przestrzeni 
w której szukamy ekstremum funkcji celu – ma ona bardzo skomplikowany kształt. Sieć uczyliśmy przez 50 epok

Poniżej znajduje się wykresy top1 oraz top5.

![image info](images/1_new.png)

Powyższe wykresy pokazują, że z kolejnymi epokami dokładność trenowania wzrasta. Na wykresie 2 widać efekt nadmiernego dopasowania, najprawdopodobniej wynika to z faktu, że liczność zbioru trenującego nie jest odpowiednio duża w stosunku do liczby klas.
Warto zwrócić uwagę na wykres 3. Widać na nim znacznie szybszy wzrost wartości top5 w stosunku do wartości top1.
Mimo, że dokłaność poprawnej odpowiedzi jest na dość niskim poziomie, to zauważalny wzrost wartości top5 w skali kolejnych epok, pozwala wnioskować, że w przyszłości wartość top1 również osiągneła by większe wartości. 

## Zadanie 2
Kolejne zadanie polegało na modyfikacji sposobu uczenia sieci. Tym razem uczyliśmy też ostatnią warstwę splotową sieci
wraz z naszą warstwą kompletnie połączoną. Sieć uczyliśmmy przez 50 epok z parametrami tymi samymi co w poprzednim zadaniu. 
Takie same były też kryteria oceny jakości.


Poniżej znajduje się wykresy top1 oraz top5.

![image info](images/2_new.png)

Wykres pokazuje, iż dodanie ostatniej wartstwy z sieci VGG do trenowania daje nie tylko gorsze rezultaty od sytuacji pokazanej w zadaniu 1, ale również dość nieprzewidywalne. Wynika to z faktu, że warstwa ta ulega na początku uczenia dość gwałtownym zmianom wartości wag. Wagi te mogą co jakiś czas nieco przybliżać się do tych wytrenowanych na zbiorze ImageNet i wówczas dawać nieco lepszy wynik.
Na podstawie uzyskanych danych można wywnioskować również, że lepiej zachowują się sieci wytrenowane w całości niż takie, w których każda z wartstw jest trenowana osobno.

Mimo faktu, że uzyskane wyniki nie są bardzo zadowalające, dokładna analiza wykresu pozwala na zauważenie trendu wzrostowego wobec funkcji dokładności od epoki.

## Zadanie 3

W trzecim zadaniu poddaliśmy całą sieć uczeniu (VGG-16 plus nasza warstwa połączona kompletnie).

Poniżej znajduje się wykresy top1 oraz top5.

![image info](images/3_new.png)


Kolejnym krokiem było usunięcie ostatniej warstwy splotowej z vgg-16 a nastepnie ponowienie uczenia. 

Poniżej znajduje się wykresy top1 oraz top5.

![image info](images/3b_new.png)


Kolejne warstwy części splotowej powinny zajmować sie rozpoznawaniem coraz bardziej abstrakcyjnych cech obrazu, więc jakość klasyfikacji jest mniejsza.

## Zadanie 4
Ostatnie zadanie polegało na użyciu na wyjściu sieci klasyfikatora SVM zamiast warstwy kompletnie połączonej w celu dokonania ostatecznej klasyfikacji.
Dla svm użyliśmy f jądra:  ["linear", "rbf", "poly"]. Kod rozwiązania jest zawarty w pliku `main.ipynb` jednak długi czas wykonywania obliczeń
zmusił nas do zrezygnowania z prezentacji wyników tego zadania, gdyż przez oczekiwanie na zakończenie obliczeń nie wyrobilibyśmy się przed umówionym terminem wysłania projektu.
 
## Wnioski

### Wyniki
W związku, że do realizacji projektu zostały użyte domowe zasoby sprzętowe (nie wspomagaliśmy się zewnętrznymi usługami chmurowymi), osiągnięte wyniki są akceptowalne,
mimo że dałoby się uzyskać lepsze rezultaty.

Istnieją metody, które usprawniają proces uczenia sieci jak np. data augmentaion, jednakże ze względu na dużą złożoność obliczeniową zrezygnowaliśmy z jej użycia.

Dość problematyczną kwestią okazał się stosunkowo liczny zbiór klas, który zawiera 105 kategorii obrazów wejściowych.
Problem ten ujawnił się szczególnie w realizacji zadania 4, ponieważ metody wektorowe SVM przystosowane są w głównej mierze do klasyfikacji binarnych. Rozwiązaniem tego problemu było zastosowanie komitetu ovo wielu svm w celu rozszerzenia możliwości klasyfikatora na wiele klas.

### Użyte technologie
Do realizacji zadania projektowego wykorzystany został pakiet Keras z językiem Python. Do najważniejszych zalet tej biblioteki należą:

- możliwość bezproblemowego uruchamiania tego samego kodu na procesorach CPU i GPU;
- przyjazny interfejs programistyczny ułatwiający szybkie prototypowanie modeli uczenia głębokiego;
- wbudowana obsługa sieci konwolucyjnych służących do przetwarzania obrazu, sieci rekurencyjnych służących do przetwarzania danych sekwencyjnych i sieci będących połączeniem obu tych rozwiązań;
- obsługa sieci o dowolnych architekturach.
