import errno

import tensorflow as tf
import numpy as np
import keras
import os
import shutil

# fractions
train_data = 0.6
validate_data = 0.2
test_data = 1.0 - validate_data - train_data
print("Data is split for " + str(100 * train_data) + "% of training data " + str(
    100 * validate_data) + "% of validating data " + str(100 * test_data) + "% of testing data.")


# Number of Folders calculation
def count_folders(path):
    count = 0
    data_in_folder = {}
    for root, dirs, files in os.walk(path):
        if root != path:
            file_name = root.replace(path, "")
            data_in_folder[file_name] = len(files)
        else:
            pass
        count += len(dirs)
    return count, data_in_folder


data_path = 'data'
numberLabels, dataInFolder = count_folders(data_path)
print(numberLabels)
print(dataInFolder)


# Number of Folders calculation
def split_data(path, path_data):
    for root, dirs, files in os.walk(path):
        if root != path:
            icon_type_name = os.path.basename(root)
            num_files_in_folder = len(files)
            num_training_data = round(train_data * num_files_in_folder)
            num_validate_data = round(validate_data * num_files_in_folder)
            num_test_data = round(test_data * num_files_in_folder)
            file_name = root.replace(path, "")
            print(str(num_files_in_folder) + " " + file_name + " training data: " + str(
                num_training_data) + " validating data: "
                  + str(num_validate_data) + " test data: " + str(num_test_data))

            for i in range(num_training_data):
                path = root + "/" + files[i]
                dest_path = path_data + "train/" + icon_type_name+"/" + files[i]
                if not os.path.exists(os.path.dirname(dest_path)):
                    try:
                        os.makedirs(os.path.dirname(dest_path))
                    except OSError as exc:  # Guard against race condition
                        if exc.errno != errno.EEXIST:
                            raise
                shutil.copyfile(root + "/" + files[i], dest_path)

            index_end_validate_data = num_training_data + num_validate_data

            print("!!!!!!!!!!!!!!!!!!" + str(index_end_validate_data))
            for i in range(num_training_data, index_end_validate_data, 1):
                path = root + "/" + files[i]
                dest_path = path_data + "validation/" + icon_type_name + "/" + files[i]
                if not os.path.exists(os.path.dirname(dest_path)):
                    try:
                        os.makedirs(os.path.dirname(dest_path))
                    except OSError as exc:  # Guard against race condition
                        if exc.errno != errno.EEXIST:
                            raise
                shutil.copyfile(root + "/" + files[i], dest_path)

            for i in range(index_end_validate_data, num_files_in_folder, 1):
                path = root + "/" + files[i]
                dest_path = path_data + "test/" + icon_type_name + "/" + files[i]
                if not os.path.exists(os.path.dirname(dest_path)):
                    try:
                        os.makedirs(os.path.dirname(dest_path))
                    except OSError as exc:  # Guard against race condition
                        if exc.errno != errno.EEXIST:
                            raise
                shutil.copyfile(root + "/" + files[i], dest_path)


data_path = 'data/'
tensor_data = 'tensor_data/'
split_data(data_path, tensor_data)
